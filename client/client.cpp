#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <netdb.h>
#include <pthread.h>
#include <set>

using namespace std;

#define PORT 1234
#define SERVERADDR "localhost"


//Создание сокета
int createSocket(){
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror("Socket creating error");
        exit(1);
    }
    return sock;
}

//Подключение клиента к серверу
void connection(int sock){
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    hostent *hst = gethostbyname(SERVERADDR);
    if (inet_addr(SERVERADDR) != INADDR_NONE)
        addr.sin_addr.s_addr = inet_addr(SERVERADDR);
    else ((unsigned long*)&addr.sin_addr)[0] = ((unsigned long**)hst->h_addr_list)[0][0];
    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Connection error");
        exit(2);
    }
}

//Отправка данных
void sendMessage(int sock, string request){
    char buff[request.size() + 1];
    strcpy(buff, request.c_str());
    long sender = send(sock, (char *)&buff, sizeof(buff), 0);
    if (sender <= 0){
        perror("Sending error");
        exit(1);
    }
}

//Получение данных
string getMessage(int sock){
    char buff[1024];
    long receiver = recv(sock, (char *)&buff, sizeof(buff), 0);
    if (receiver < 0){
        perror("Receiving error");
        exit(1);
    }
    if (receiver == 0) return "";
    return (string)buff;
}

void* chat(void* memberSock){
    int sock = *((int*)memberSock);
    while(true) {
        string message = getMessage(sock);
        if (message != "") cout << message << endl;
    }
    //close(sock);
    return 0;
}

void authorization(int sock){
    string nick;
    
    while (true){
        cout << "Enter nick: ";
        cin >> nick;
        sendMessage(sock, nick);
        string answer = getMessage(sock);
        if (answer == "TRUE") {
            cout << "Nickname is available! You entered the chat.\n\nCommands:\n-Enter DISCONNECT to disconnect the chat.\n-Enter ! + nick for private mode.\n";
            break;
        }
        if (answer == "FALSE") cout << "Nickname is unavailable! Try again.\n";
    }
}

void getMembersList(int sock){
    string list = getMessage(sock);
    cout << "\nMembers list: " + list << endl;
}

int main(int argc, const char * argv[]) {
    int sock = createSocket();
    connection(sock);
    authorization(sock);
    getMembersList(sock);
    cout << "Start messaging: \n";
    
    pthread_t thread_ptr;
    pthread_create(&thread_ptr, NULL, chat, (void*)&sock);
    
    while (true) {
        string message;
        getline(cin, message);
        sendMessage(sock, message);
        if (message == "DISCONNECT") break;
    }
    
    close(sock);
    cin.get();
    return 0;
}
