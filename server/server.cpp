#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <netdb.h>
#include <pthread.h>
#include <map>
#include <set>

using namespace std;

#define PORT 1234

//set<int> members;
//set<string> nicknames;

map<string, int> members;

//Создание TCP сокета
int createSocket(){
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("Socket creating error");
        exit(1);
    }
    return sock;
}

//Связывание сокета с локальным адресом
void binding(int sock){
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = 0;
//    Предотвращение ошибки "Address already in use" и ожидания повторного открытия порта (TIME_WAIT)
    int option = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)) == -1) perror("setsockopt");
    if(::bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0){
        perror("Binding error");
        exit(1);
    }
}

//Принятие нового участника
int acception(int sock){
    struct sockaddr_in clientAddr;
    socklen_t clientAddrSize = sizeof(clientAddr);
    int sockAccept = accept(sock, (struct sockaddr *)&clientAddr, &clientAddrSize);
    if(sockAccept < 0){
        perror("Accepting error");
        exit(1);
    }
    hostent *hst = gethostbyaddr((char*)&clientAddr.sin_addr, 4, AF_INET);
    cout << "\nNew connection!\n";
    cout << ((hst) ? hst->h_name : "Unknown host") << "/" << inet_ntoa(clientAddr.sin_addr) << "/" << ntohs(clientAddr.sin_port) << endl;
    return sockAccept;
}

//Отправка данных
void sendMessage(int sock, string request){
    char buff[request.size() + 1];
    strcpy(buff, request.c_str());
    cout << (string) buff;
    long sender = send(sock, (char *)&buff, sizeof(buff), 0);
    if (sender <= 0){
        perror("Sending error");
        exit(1);
    }
}

//Получение данных
string getMessage(int sock){
    char buff[1024];
    long receiver = recv(sock, (char *)&buff, sizeof(buff), 0);
    if (receiver < 0){
        perror("Receiving error");
        exit(1);
    }
    if (receiver == 0) return "";
    return (string)buff;
}

bool checkNickname(string nick) {
    if (members.find(nick) == members.end()) return 1;
    return 0;
}

//Получение валидного ника
string getNickname(int sock) {
    string nick;
    while (true) {
        nick = getMessage(sock);
        if (checkNickname(nick)) {
            sendMessage(sock, "TRUE");
            members[nick] = sock;
            break;
        }
        sendMessage(sock, "FALSE");
    }
    return nick;
}

void sendMembersList(int sock) {
    string list = "\n";
    for (auto member : members)
        list += member.first + "\n";
    //cout << list;
    
    sendMessage(sock, list);
}

void* ConToClient(void* clientSock) {
    int sock = *((int*)clientSock);
    
    string nick = getNickname(sock);
    sendMembersList(sock);
    
    //Рассылка сообщения о подключении нового человека
    for (auto member : members)
        if (member.second != sock)
            sendMessage(member.second, nick + " CONNECTED");
    
    //Чат
    while(true) {
        string message = getMessage(sock);
        if (message == "DISCONNECT"){
            for (auto member : members)
                if (member.second != sock)
                    sendMessage(member.second, nick + " DISCONNECTED");
            members.erase(nick);
            break;
        }
        if (message[0] == '!') {
            string privateNick = message.substr(1, message.find(" ") - 1);
            string privateMessage = "[PRIVATE] " + nick + ": " +
            message.substr(message.find(" ") + 1);
//            cout << privateNick <<"123  " << members[privateNick];
//            cin.get();
            
            if (members.find(privateNick) != members.end())
                sendMessage(members[privateNick], privateMessage);
            else {
                for (auto member : members)
                    if (member.second != sock)
                        sendMessage(member.second, nick + ": " + message);
            }
        }
        else if (message != ""){
            for (auto member : members)
                if (member.second != sock)
                    sendMessage(member.second, nick + ": " + message);
        }
    }
    close(sock);
    return 0;
}

int main(int argc, const char * argv[]) {
    cout << "Server is working!\n";
    int sock = createSocket();
    binding(sock);
    listen(sock, 0x100);
    cout << "Listening...\n";
    
    while (true){
        int member = acception(sock);
        pthread_t thread_ptr;
        pthread_create(&thread_ptr, NULL, ConToClient, (void*)&member);
    }
    
    //close(sock);
    return 0;
}
